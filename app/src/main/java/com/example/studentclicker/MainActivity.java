package com.example.studentclicker;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private TextView students;
    private Integer counter = 0;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        students = findViewById(R.id.txt_counter);

        setText(students, String.valueOf(counter));
    }

    /**
     * Вызывается перед выходом из активного состояния,
     * позволяя сохранить состояние в объекте savedInstanceState
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt("counter", counter);
        setText(students, String.valueOf(counter));
    }

    /**
     * Вызывается после завершения метода onCreate
     * Используется для восстановления состояния UI
     * @param savedInstanceState - объект, который необходимо восстановить
     */
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if (savedInstanceState.containsKey("counter")) {
            counter = savedInstanceState.getInt("counter");
        }
        setText(students, String.valueOf(counter));
    }

    public void onClickBtnAddStudents(View view) {
        counter++;
        setText(students, String.valueOf(counter));
        if (counter == 2147483647) {
            Toast.makeText(this, getString(R.string.txt_win) +
                    getString(R.string.spacing) + getString(R.string.txt_reset),
                    Toast.LENGTH_SHORT).show();
            counter = 0;
        }
    }

    public void onClickBtnReset(View view) {
        Toast.makeText(this, getString(R.string.txt_reset), Toast.LENGTH_SHORT).show();
        counter = 0;
        setText(students, String.valueOf(counter));
    }
    /**
     * Метод предназначен для отображения счёта в TextView
     *
     * @param view элемент TextView, на котором будет отображаться счёт
     * @param counter счётчик
     */
    private void setText(TextView view, String counter) {
        view.setText(counter);
    }
}